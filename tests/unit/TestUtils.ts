import sinon, { SinonStub } from 'sinon';

import { AxiosError, AxiosInstance } from 'axios';

export interface AxiosInstanceStub extends AxiosInstance {
  get: SinonStub;
  post: SinonStub;
  put: SinonStub;
  delete: SinonStub;
}

export const stubAxiosInstance = (): AxiosInstanceStub =>
  ({
    get: sinon.stub(),
    post: sinon.stub(),
    put: sinon.stub(),
    delete: sinon.stub(),
  } as any);

export const axiosError = (code: number = 500, message: string = 'Network Error'): AxiosError =>
  ({
    message: message,
    status: code,
    response: {
      code: code,
      status: code,
      message: message,
      stack: `Error ${message}`,
      data: { message: message },
    },
  } as any);

export const dataSelector = (selector: string): string => {
  return `[data-selector="${selector}"]`;
};

//@ts-ignore
export const flushPromises = () => new Promise(setImmediate);
