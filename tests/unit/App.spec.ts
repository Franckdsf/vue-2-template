import { shallowMount, Wrapper } from '@vue/test-utils';

import { AppComponent, AppVue } from '@/primary/app';

let wrapper: Wrapper<AppComponent>;
let component: AppComponent;

const wrap = () => {
  wrapper = shallowMount(AppVue, { stubs: ['router-link', 'router-view'] });
  component = wrapper.vm;
};

describe('App', () => {
  it('Show successfully create an App', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });
});
