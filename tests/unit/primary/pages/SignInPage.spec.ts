import { Wrapper, shallowMount } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { resolveRestAccountRepository, rejectRestAccountRepository } from '../../fixtures/RestAccountRepository.fixture';
import { dataSelector, flushPromises } from '../../TestUtils';

import { AccountRepository } from '@/domain/account/AccountRepository';
import { SignInPageComponent, SignInPageVue } from '@/primary/pages/signInPage';

let wrapper: Wrapper<SignInPageComponent>;
let component: SignInPageComponent;

const wrap = (overrideAccountRepository?: AccountRepository) => {
  const accountRepository = overrideAccountRepository ? overrideAccountRepository : resolveRestAccountRepository();

  wrapper = shallowMount<SignInPageComponent>(SignInPageVue, {
    provide: {
      accountRepository: () => accountRepository,
    },
  });
  component = wrapper.vm;
};

describe('Sign in page', () => {
  it('Should successfully create a component', () => {
    wrap();
    expect(wrapper.exists()).toBeTruthy();
  });

  it('Should successfully try to log in when connect button pressed', async () => {
    const accountRepository = resolveRestAccountRepository();
    wrap(accountRepository);

    wrapper.find(dataSelector('signInPage-email')).setValue('someEmail');
    expect(component.email).toBe('someEmail');

    wrapper.find(dataSelector('signInPage-password')).setValue('somePassword');
    expect(component.password).toBe('somePassword');

    component.signIn = jest.fn();
    wrapper.find(dataSelector('signInPage-connect')).trigger('click');
    expect(component.signIn).toBeCalled();
  });

  it('Should successfully log in', async () => {
    const accountRepository = resolveRestAccountRepository();
    const signIn = jest.spyOn(accountRepository, 'signIn');
    wrap(accountRepository);

    component.email = 'someEmail';
    component.password = 'somePassword';
    component.signIn();

    expect(component.status).toBe('pending');
    expect(signIn).toBeCalledWith('someEmail', 'somePassword');
    await flushPromises();
    expect(component.status).toBe('success');
  });

  it('Should fail to log in', async () => {
    const accountRepository = rejectRestAccountRepository();
    const signIn = jest.spyOn(accountRepository, 'signIn');
    wrap(accountRepository);

    component.email = 'someEmail';
    component.password = 'somePassword';
    component.signIn();

    expect(component.status).toBe('pending');
    expect(signIn).toBeCalledWith('someEmail', 'somePassword');
    await flushPromises();
    expect(component.status).toBe('error');
    expect(component.error).not.toBe('');
    expect(wrapper.find(dataSelector('signInPage-error')).text()).toBe(component.error);
  });
});
