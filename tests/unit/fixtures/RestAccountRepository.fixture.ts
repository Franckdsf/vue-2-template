import sinon, { SinonStub } from 'sinon';

import { AxiosError } from 'axios';

import { axiosError } from '../TestUtils';

import { fixtureAccount } from './Account.fixture';

export interface RestAccountRepositoryStub {
  get: SinonStub;
  signIn: SinonStub;
}

export const stubRestAccountRepository = (): RestAccountRepositoryStub => ({
  get: sinon.stub(),
  signIn: sinon.stub(),
});

export const resolveRestAccountRepository = (): RestAccountRepositoryStub => {
  const _stubRestAccountRepository = stubRestAccountRepository();
  _stubRestAccountRepository.get.resolves(fixtureAccount());
  _stubRestAccountRepository.signIn.resolves(true);
  return _stubRestAccountRepository;
};

export const rejectRestAccountRepository = (overrideError?: AxiosError): RestAccountRepositoryStub => {
  const error = overrideError ? overrideError : axiosError(500, 'Network error');
  const _stubRestAccountRepository = stubRestAccountRepository();
  _stubRestAccountRepository.get.rejects(error);
  _stubRestAccountRepository.signIn.rejects(error);
  return _stubRestAccountRepository;
};
