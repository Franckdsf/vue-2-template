import { Component, Vue, Inject } from 'vue-property-decorator';

import { AccountRepository } from '@/domain/account/AccountRepository';

@Component({})
export default class SignInPageComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  email: string = '';
  password: string = '';

  status: string = 'success';
  error: string = '';

  signIn() {
    this.status = 'pending';
    this.accountRepository()
      .signIn(this.email, this.password)
      .then(() => {
        this.status = 'success';
      })
      .catch(error => {
        this.status = 'error';
        this.error = error.toString();
      });
  }
}
