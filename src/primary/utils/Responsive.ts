export class Responsive {
  constructor(private globalWindow: Window) {}

  get isMobile(): boolean {
    return this.globalWindow.innerWidth < 1100;
  }
}
