import { AxiosInstance, AxiosResponse } from 'axios';

import { RestAccount, toAccount } from './RestAccount';

import { Account } from '@/domain/account/Account';
import { AccountRepository } from '@/domain/account/AccountRepository';

export class RestAccountRepository implements AccountRepository {
  constructor(private axiosInstance: AxiosInstance) {}

  get(username: string): Promise<Account> {
    return this.axiosInstance
      .get(`/user/${username}`)
      .then((response: AxiosResponse<RestAccount>) => toAccount(response.data))
      .catch(error => {
        throw new Error(`Can't get account: ${error.message}`);
      });
  }
  signIn(username: string, password: string): Promise<boolean> {
    return this.axiosInstance
      .post('/user', { username: username, password: password })
      .then((response: AxiosResponse<RestAccount>) => true)
      .catch(error => {
        if (error.status == 402) throw new Error("Can't sign in: email or password incorrect");
        else throw new Error(`Can't sign in: ${error.message}`);
      });
  }
}
