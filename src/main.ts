import axios from 'axios';
import Vue from 'vue';

import { RestAccountRepository } from './secondary/restAccount/RestAccountRepository';

import App from '@/primary/app/App.vue';
import router from '@/router';
import store from '@/secondary/store';

Vue.config.productionTip = false;

const axiosInstance = axios.create({
  baseURL: 'http://someUrl.com',
});

const accountRepository = new RestAccountRepository(axiosInstance);

new Vue({
  router: router,
  store: store,
  render: h => h(App),
  provide: {
    accountRepository: () => accountRepository,
    // import jquery
    // $: () => require('jquery'),
  },
}).$mount('#app');
